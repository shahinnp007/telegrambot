FROM maven:3.6.2-jdk-11 AS MAVEN_BUILD

MAINTAINER Shahin

COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/
RUN mvn package

FROM adoptopenjdk/openjdk11:latest

WORKDIR /build/

COPY --from=MAVEN_BUILD /build/target/telegrambot-0.0.1-SNAPSHOT.jar /build/

ENTRYPOINT ["java", "-jar", "telegrambot-0.0.1-SNAPSHOT.jar"]