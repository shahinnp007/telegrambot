package com.shahinnp.telegrambot;

import java.util.Map;
import java.util.TreeMap;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Component
public class AmazonApiWitoutSDK {

	private static final String HOST = "webservices.amazon.in";
	private static final String URI_PATH = "/paapi5/searchitems";
	private static final String ACCESS_KEY = "AKIAJ2I7R7XAFHFCQPPQ";
	private static final String SECRET_KEY = "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";
	private static final String REGION = "eu-west-1";

	public String sendOffers(String requestString, String pageNumber) {
		

		String[] payloadArray = requestString.split("/");

		System.out.println(payloadArray);

		String requestPayload = "{" ;
		if(!payloadArray[0].equals("#")) {
		requestPayload+= " \"Title\": \"" + payloadArray[0] + "\","; 
		}
		if(!payloadArray[1].equals("#")) {
		requestPayload+= " \"Keywords\": \"" + payloadArray[1] + "\","; 
		}
		if(!payloadArray[2].equals("#")) {
		requestPayload+= " \"SearchIndex\": \""+ payloadArray[2] + "\",";
		}
		requestPayload+= " \"Resources\": [" ;
		requestPayload+= "  \"CustomerReviews.StarRating\",";
		requestPayload+= "  \"Images.Primary.Large\",";
		requestPayload+= "  \"ItemInfo.ByLineInfo\",";
		requestPayload+= "  \"ItemInfo.ProductInfo\",";
		requestPayload+= "  \"ItemInfo.Title\"," + "  \"Offers.Listings.Price\",";
		requestPayload+= "  \"Offers.Summaries.HighestPrice\",";
		requestPayload+= "  \"Offers.Summaries.LowestPrice\"" + " ],";
		requestPayload+= " \"Condition\": \"New\"," ;
		if(!payloadArray[3].equals("#")) {
		requestPayload+= " \"MinReviewsRating\": "+ payloadArray[3] + ",";
		}
		if(!payloadArray[4].equals("#")) {
		requestPayload+= " \"SortBy\": \""+payloadArray[4]+"\"," ;
		}
		if(!payloadArray[5].equals("#")) {
		requestPayload+= " \"MinSavingPercent\": " + payloadArray[5] + ",";
		}
		if(!payloadArray[6].equals("#")) {
		requestPayload+= " \"MinPrice\": "+ payloadArray[6] +"," ;
		}
		if(!payloadArray[7].equals("#")) {
		requestPayload+= " \"MaxPrice\": "+ payloadArray[7] +"," ;
		}
		if(!payloadArray[8].equals("#")) {
		requestPayload+= " \"ItemCount\": "+ payloadArray[8] +",";
		}
		if(!payloadArray[9].equals("#")) {
		requestPayload+= " \"ItemPage\": "+ pageNumber +",";
		}
		requestPayload+= " \"PartnerTag\": \"shafunint-21\"," + " \"PartnerType\": \"Associates\",";
		requestPayload+= " \"Marketplace\": \"www.amazon.in\"" + "}";

		TreeMap<String, String> headers = new TreeMap<String, String>();
		headers.put("host", HOST);
		headers.put("content-type", "application/json; charset=UTF-8");
		headers.put("x-amz-target", "com.amazon.paapi5.v1.ProductAdvertisingAPIv1.SearchItems");
		headers.put("content-encoding", "amz-1.0");

		AWSV4Auth awsv4Auth = new AWSV4Auth.Builder(ACCESS_KEY, SECRET_KEY).path(URI_PATH).region(REGION)
				.service("ProductAdvertisingAPI").httpMethodName("POST").headers(headers).payload(requestPayload)
				.build();
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost("https://" + HOST + URI_PATH);

			httpPost.setEntity(new StringEntity(requestPayload));

			Map<String, String> header = awsv4Auth.getHeaders();
			for (Map.Entry<String, String> entrySet : header.entrySet()) {
				httpPost.addHeader(entrySet.getKey(), entrySet.getValue());
			}

			HttpResponse response = client.execute(httpPost);
			HttpEntity entity = response.getEntity();
			String jsonResponse = EntityUtils.toString(entity, StandardCharsets.UTF_8);
			int statusCode = response.getStatusLine().getStatusCode();
			System.out.println(jsonResponse);
			if (statusCode == 200) {
				System.out.println("Successfully received response from Product Advertising API.");
				System.out.println(jsonResponse);
				return jsonResponse;

			} else {
				JSONObject json = new JSONObject(jsonResponse);
				if (json.has("Errors")) {
					JSONArray errorArray = json.getJSONArray("Errors");
					for (int i = 0; i < errorArray.length(); i++) {
						JSONObject e = errorArray.getJSONObject(i);
						System.out.println("Error Code: " + e.get("Code") + ", Message: " + e.get("Message"));
					}
				} else {
					System.out.println(
							"Error Code: InternalFailure, Message: The request processing has failed because of an unknown error, exception or failure. Please retry again.");
				}
			}

		} catch (Exception e) {
			return "";
		}
		return "";

	}
	
}
