package com.shahinnp.telegrambot;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendAnimation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.send.SendVideo;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import com.shahinnp.telegrambot.entity.FilterContent;
import com.shahinnp.telegrambot.entity.PostQueue;
import com.shahinnp.telegrambot.repository.FilterContentRepository;
import com.shahinnp.telegrambot.repository.PostQueueRepository;




@Component
public class SimpleBot extends TelegramLongPollingBot {

	@Autowired
	GraphPostUtitlty grapUtility;

	@Value("${bot.name}")
	String botName;

	@Value("${chat.grouptest}")
	String groupTestId;

	@Value("${chat.groupprod}")
	String groupProdId;

	@Value("${chat.botid}")
	String groupBotId;

	@Value("${chat.me}")
	String chatMe;
	
	
	@Value("${bot.type}")
	String botType;

	@Autowired
	AmazonApiWitoutSDK amzWithoutSDK;

	@Autowired
	private RestTemplate resttemp;

	@Autowired
	private PostQueueRepository postQueueRep;
	
	@Autowired
	private FilterContentRepository filterRep; 
	
	public SimpleBot() {

		super("6460375076:AAGzQIYy-e3QLD2Mk8UjfS_OlAo9crsfVPU");
	}

	@Override
	public void onUpdateReceived(Update update) {
		String rrStrg = update.getMessage().getText();

		long chatId = update.hasMessage() 
		           ? update.getMessage().getChat().getId() 
		           : update.getCallbackQuery().getMessage().getChat().getId();
		
		if(!String.valueOf(chatId).equals(chatMe) && !String.valueOf(chatId).equals("6622222390")) {
			return;
		}
			
		if (botType.equals("TGMBULK") && rrStrg != null && rrStrg.startsWith("*offers")
				&& rrStrg.split("-")[0].equals("*offers")) {

			String[] payloadArray = rrStrg.split("-")[1].split("/");

			for (int k = 1; k <= Integer.parseInt(payloadArray[9]); k++) {
				try {
					if (k != 1) {
						TimeUnit.MINUTES.sleep(1);
					}
				} catch (InterruptedException e1) {

				}
				JSONObject json = new JSONObject(amzWithoutSDK.sendOffers(rrStrg.split("-")[1], k + ""));

				try {
					JSONArray items = json.getJSONObject("SearchResult").getJSONArray("Items");

					for (int i = 0; i < items.length(); i++) {
						JSONObject item = items.getJSONObject(i);

						String title = item.getJSONObject("ItemInfo").getJSONObject("Title").getString("DisplayValue");
						String detailPageURL = item.getString("DetailPageURL");

						String urlImage = item.getJSONObject("Images").getJSONObject("Primary").getJSONObject("Large")
								.getString("URL");

						JSONArray offers = item.getJSONObject("Offers").getJSONArray("Listings");
						JSONArray summeries = item.getJSONObject("Offers").getJSONArray("Summaries");
						String dealPrice = offers.getJSONObject(0).getJSONObject("Price").getString("DisplayAmount");
						Long dealPriceAmout = offers.getJSONObject(0).getJSONObject("Price").getLong("Amount");
						Long regularPrice = summeries.getJSONObject(0).getJSONObject("HighestPrice").getLong("Amount");
						String regularPriceDisplay = summeries.getJSONObject(0).getJSONObject("HighestPrice")
								.getString("DisplayAmount");

						Long lowestPrice = summeries.getJSONObject(0).getJSONObject("LowestPrice").getLong("Amount");

						

						SendPhoto sendPhoto = new SendPhoto();
						sendPhoto.setPhoto(new InputFile(urlImage));
						if (rrStrg.split("-")[3].toString().equals("PROD")) {
							sendPhoto.setChatId(groupProdId);
						} else {
							sendPhoto.setChatId(groupTestId);
						}

						sendPhoto.setParseMode("HTML");
						StringBuilder strText = new StringBuilder();

						if (dealPriceAmout < lowestPrice) {
							strText.append("🔥🔥 DEAL OF THE DAY 🔥🔥\n\n");
						}

						strText.append("💥" + title + " \n\n").append("✅ Offer Price: <b>" + dealPrice + "</b>\n");
						if (!dealPriceAmout.equals(regularPrice)) {
							strText.append("❌ Regular Price: <b>" + regularPriceDisplay + "</b> \n\n");
						}

						if (dealPriceAmout <= lowestPrice) {
							strText.append("<b><i>😱 Lowest Price in History</i></b> \n\n");
						}

						strText.append("🔗 <a href=\"" + detailPageURL + "\">Shop Now @ ");
						strText.append(dealPrice);
						strText.append("</a>");
						strText.append("\n\n Join @premiumdeals777");

						// send.setText(strText.toString());
						sendPhoto.setCaption(strText.toString());
						try {

							// execute(send);
							execute(sendPhoto);
							SendMessage send = new SendMessage();
							send.setChatId(groupBotId);
							send.setText((i + 1) + " of " + items.length() + " Product Published to Group");
							execute(send);
							TimeUnit.MINUTES.sleep(Long.parseLong(rrStrg.split("-")[2]));
						} catch (TelegramApiException e) {
							e.printStackTrace();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

					SendMessage send = new SendMessage();
					send.setChatId(groupBotId);
					send.setText("All Product Page" + k + " Published to Group"
							+ "\n\nThe Format request of Request\r\n" + "\r\n" + "  Slashes between each property\r\n"
							+ "\r\n" + "1.Title\r\n" + "2.Keywords \r\n" + "3.SearchIndex - All\r\n"
							+ "4.MinReviewsRating\r\n" + "5.SortBy - Featured/NewestArrivals/Relevance/\r\n"
							+ "           Price:HighToLow/Price:LowToHigh\r\n" + "6.MinSavingPercent\r\n"
							+ "7.MinPrice\r\n" + "8.MaxPrice\r\n" + "9.ItemCount\r\n" + "10.ItemPage\r\n" + "\r\n"
							+ "After Hiphen\r\n" + "\r\n" + "1.TimeLag\r\n" + "2.PROD/TEST");

					try {
						execute(send);
					} catch (TelegramApiException e) {

						e.printStackTrace();
					}
				} catch (Exception e) {
					SendMessage send = new SendMessage();
					send.setChatId(groupBotId);
					send.setText("No Result" + "\n\nThe Format request of Request\r\n" + "\r\n"
							+ "  Slashes between each property\r\n" + "\r\n" + "1.Title\r\n" + "2.Keywords \r\n"
							+ "3.SearchIndex - All\r\n" + "4.MinReviewsRating\r\n"
							+ "5.SortBy - Featured/NewestArrivals/Relevance/\r\n"
							+ "           Price:HighToLow/Price:LowToHigh\r\n" + "6.MinSavingPercent\r\n"
							+ "7.MinPrice\r\n" + "8.MaxPrice\r\n" + "9.ItemCount\r\n" + "10.ItemPage\r\n" + "\r\n"
							+ "After Hiphen\r\n" + "\r\n" + "1.TimeLag\r\n" + "2.PROD/TEST");

					try {
						execute(send);
						break;
					} catch (TelegramApiException ex) {

						ex.printStackTrace();
					}
				}
			}
		} else if (!botType.equals("TGMBULK")) {

			String inputText;
			List<MessageEntity> tempCaptionList = new ArrayList<>();
			

			if (update.hasMessage() && update.getMessage().hasText()) {
				inputText = update.getMessage().getText();
				tempCaptionList = update.getMessage().getEntities();
			} else {
				inputText = update.getMessage().getCaption();
				tempCaptionList = update.getMessage().getCaptionEntities();
			}
			StringBuilder newString = new StringBuilder();

			List<MessageEntity> captionList = new ArrayList<MessageEntity>();
			if (tempCaptionList != null) {
				captionList = tempCaptionList.stream().filter(x -> x.getType().equals("text_link"))
						.collect(Collectors.toList());
			}
			int offset = 0;

			for (int i = 0; i < captionList.size(); i++) {

				newString.append(inputText.substring(offset, captionList.get(i).getOffset()));

				newString.append(captionList.get(i).getUrl());

				offset = captionList.get(i).getOffset() + captionList.get(i).getLength();
			}
			
			if (!captionList.isEmpty()) {
				newString.append(inputText.substring(offset));
			} else {
				newString.append(inputText);
			}

			List<FilterContent> listFilter=filterRep.findAll();
			
			for(FilterContent x:listFilter) {
			String matchStr=x.getFilterText().replace("\r", "");

				if(newString.toString().contains(matchStr)) {
					
					String temp=(newString.toString().replace(matchStr,x.getReplaceWith()));
					newString.setLength(0);
					newString.append(temp);
					
				}
				
				
			}
			
			String urlPattern = "(https?://\\S+)";
			Pattern pattern = Pattern.compile(urlPattern);
			Matcher matcher = pattern.matcher(newString);

			StringBuffer modifiedText = new StringBuffer();
			while (matcher.find()) {
				String originalUrl = matcher.group();
				String alteredUrl = alterUrl(originalUrl);
				matcher.appendReplacement(modifiedText, Matcher.quoteReplacement(alteredUrl));
			}
			matcher.appendTail(modifiedText);

			Pattern temppattern = Pattern.compile(urlPattern);
			Matcher tempmatcher = temppattern.matcher(newString);
			StringBuffer modifiedTextFb = new StringBuffer();
			while (tempmatcher.find()) {
				String originalUrl = tempmatcher.group();
				String alteredUrl = alterUrlV2(originalUrl);
				tempmatcher.appendReplacement(modifiedTextFb, Matcher.quoteReplacement(alteredUrl));
			}
			tempmatcher.appendTail(modifiedTextFb);

			if (update.hasMessage() && !update.getMessage().hasText()) {
				if (update.getMessage().hasPhoto()) {
					SendPhoto sendPhoto1 = new SendPhoto();
					sendPhoto1.setPhoto(new InputFile(update.getMessage().getPhoto().get(0).getFileId()));
					sendPhoto1.setCaption(modifiedText.toString().equals("null") ? "" : modifiedText.toString());
					sendPhoto1.setChatId(groupProdId);
					sendPhoto1.setParseMode("HTML");

					try {
						if(botType.equals("TGM")) {
						execute(sendPhoto1);
						}
						if(botType.equals("FB")) {
						String fileurl = getURLWithFileID("https://api.telegram.org/bot" + getBotToken()
								+ "/getFile?file_id=" + update.getMessage().getPhoto()
										.get(update.getMessage().getPhoto().size() - 1).getFileId());
						
						String hashtags="";
						
						PostQueue p1= new PostQueue(fileurl,(modifiedTextFb.toString().equals("null") ? "" : modifiedTextFb.toString()), hashtags,"PHOTO");
						postQueueRep.save(p1);
					
						}

					} catch (TelegramApiException e) {
						e.printStackTrace();
					}
				} else if (update.getMessage().hasAnimation()) {
					SendAnimation sendAni = new SendAnimation();
					sendAni.setAnimation(new InputFile(update.getMessage().getAnimation().getFileId()));
					sendAni.setCaption(modifiedText.toString().equals("null") ? "" : modifiedText.toString());
					sendAni.setChatId(groupProdId);
					sendAni.setParseMode("HTML");
					try {
						if(botType.equals("TGM")) {
						execute(sendAni);
						}
					} catch (TelegramApiException e) {
						e.printStackTrace();
					}
				} else if (update.getMessage().hasVideo()) {
					SendVideo sendAni = new SendVideo();
					sendAni.setVideo(new InputFile(update.getMessage().getVideo().getFileId()));
					sendAni.setCaption(modifiedText.toString().equals("null") ? "" : modifiedText.toString());
					sendAni.setChatId(groupProdId);
					sendAni.setParseMode("HTML");
					try {
						if(botType.equals("TGM")) {
						execute(sendAni);
						}
						if(botType.equals("FB")) {
						String fileurl = getURLWithFileID("https://api.telegram.org/bot" + getBotToken()
								+ "/getFile?file_id=" + update.getMessage().getVideo().getFileId());
						/*sendReelsToFAcebook(fileurl,
								modifiedTextFb.toString().equals("null") ? "" : modifiedTextFb.toString());
					    */
						String hashtags="";
						
						PostQueue p1= new PostQueue(fileurl,(modifiedTextFb.toString().equals("null") ? "" : modifiedTextFb.toString()), hashtags,"VIDEO");
						postQueueRep.save(p1);
						}
					} catch (TelegramApiException e) {
						e.printStackTrace();
					}
				}

			} else {

				SendMessage sendMessag2 = new SendMessage();
				sendMessag2.setChatId(groupProdId);
				sendMessag2.setText(modifiedText.toString());
				sendMessag2.setDisableWebPagePreview(true);
				sendMessag2.setParseMode("HTML");

				try {
					execute(sendMessag2);
				} catch (TelegramApiException e) {
					e.printStackTrace();
				}

			}

		}
	}

	private String getURLWithFileID(String string) {

		JSONObject result = new JSONObject(resttemp.getForObject(string, String.class));
		String filePath = result.getJSONObject("result").getString("file_path");
		return "https://api.telegram.org/file/bot" + getBotToken() + "/" + filePath;
	}

	@Override
	public String getBotUsername() {

		return botName;
	}

	private String alterUrl(String url) {

		String expURL = expandShortUrl(url);

		StringBuilder modiURL = new StringBuilder();
		try {
			URI uri = new URI(expURL);
			String scheme = uri.getScheme();
			String host = uri.getHost();

			String path = uri.getPath();
			String queryString = uri.getQuery() != null ? uri.getQuery() : "";

			StringTokenizer str = new StringTokenizer(queryString, "&");

			if (host.equals("www.amazon.in")) {

				modiURL.append(scheme).append("://").append(host).append(path).append("?");

				while (str.hasMoreTokens()) {
					String token = str.nextToken();
					if (!token.startsWith("tag") && !token.startsWith("affid") && !token.startsWith("sid")) {
						modiURL.append(token);
						modiURL.append("&");
					}

				}

				modiURL.append("tag=shafunint-21");
				return "<a href=\"" + modiURL.toString() + "\"> Shop @ Amazon</a>";
			} else if (host.equals("www.flipkart.com") || host.equals("dl.flipkart.com")) {
				modiURL.append(scheme).append("://").append(host).append(path).append("?");

				while (str.hasMoreTokens()) {
					String token = str.nextToken();
					if (!token.startsWith("tag") && !token.startsWith("affid")) {
						modiURL.append(token);
						modiURL.append("&");
					}

				}

				modiURL.append("affid=shahinnpg");
				return "<a href=\"" + modiURL.toString() + "\"> Shop @ Flipkart</a>";

			}

			else if (host.equals("linkredirect.in") || host.equals("linkredirect.in")) {
				modiURL.setLength(0);

				while (str.hasMoreTokens()) {
					String token = str.nextToken();
					if (token.startsWith("dl=")) {

						int startIndex = queryString.indexOf("dl=");
						String tempURl = queryString.substring(startIndex + 3, queryString.length());

						URI tempuri = new URI(tempURl);
						String tempscheme = tempuri.getScheme();
						String temphost = tempuri.getHost();

						String temppath = tempuri.getPath();
						String tempqueryString = tempuri.getQuery() != null ? tempuri.getQuery() : "";

						StringTokenizer temp = new StringTokenizer(tempqueryString);

						if (temphost.equals("www.amazon.in")) {

							modiURL.append(tempscheme).append("://").append(temphost).append(temppath).append("?");

							while (temp.hasMoreTokens()) {
								String temptoken = temp.nextToken();
								if (!temptoken.startsWith("tag") && !temptoken.startsWith("affid")
										&& !temptoken.startsWith("sid")) {
									modiURL.append(temptoken);
									modiURL.append("&");
								}

							}

							modiURL.append("tag=shafunint-21");
							return "<a href=\"" + modiURL.toString() + "\"> Shop @ Amazon</a>";
						} else if (temphost.equals("www.flipkart.com")) {

							modiURL.append(tempscheme).append("://").append(temphost).append(temppath).append("?");

							while (temp.hasMoreTokens()) {
								String temptoken = temp.nextToken();
								if (!temptoken.startsWith("tag") && !temptoken.startsWith("affid")) {
									modiURL.append(temptoken);
									modiURL.append("&");
								}

							}

							modiURL.append("affid=shahinnpg");
							return "<a href=\"" + modiURL.toString() + "\"> Shop @ Flipkart</a>";
						}

					}

				}

			}
			return url;

		} catch (URISyntaxException e) {
			return url;
		}

	}

	private String expandShortUrl(String shortUrl) {
		try {
			String expandedUrl = "";
			HttpURLConnection connection = (HttpURLConnection) new URL(shortUrl).openConnection();
			connection.setInstanceFollowRedirects(false);
			connection.connect();
			if (connection.getHeaderField("Location") != null
					&& !connection.getHeaderField("Location").equals("/404")) {
				expandedUrl = connection.getHeaderField("Location");
			} else {
				expandedUrl = shortUrl;
			}

			return expandedUrl;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String alterUrlV2(String url) {

		String expURL = expandShortUrl(url);
		
		StringBuilder modiURL = new StringBuilder();
		try {
			URI uri = new URI(expURL);
			String scheme = uri.getScheme();
			String host = uri.getHost();

			String path = uri.getPath();
			String queryString = uri.getQuery() != null ? uri.getQuery() : "";

			StringTokenizer str = new StringTokenizer(queryString, "&");

			if (host.equals("www.amazon.in")) {

				modiURL.append(scheme).append("://").append(host).append(path).append("?");

				while (str.hasMoreTokens()) {
					String token = str.nextToken();
					if (!token.startsWith("tag") && !token.startsWith("affid") && !token.startsWith("sid")) {
						modiURL.append(token);
						modiURL.append("&");
					}

				}

				modiURL.append("tag=shafunint-21");
				return "https://xtrashop.in/" + grapUtility.createShortURL(modiURL.toString());
			} else if (host.equals("www.flipkart.com") || host.equals("dl.flipkart.com")) {
				modiURL.append(scheme).append("://").append(host).append(path).append("?");

				while (str.hasMoreTokens()) {
					String token = str.nextToken();
					if (!token.startsWith("tag") && !token.startsWith("affid")) {
						modiURL.append(token);
						modiURL.append("&");
					}

				}

				modiURL.append("affid=shahinnpg");
				return "https://xtrashop.in/" + grapUtility.createShortURL(modiURL.toString());
			} else if (host.equals("linkredirect.in") || host.equals("linkredirect.in")) {
				modiURL.setLength(0);

				while (str.hasMoreTokens()) {
					String token = str.nextToken();
					if (token.startsWith("dl=")) {

						int startIndex = queryString.indexOf("dl=");
						String tempURl = queryString.substring(startIndex + 3, queryString.length());

						URI tempuri = new URI(tempURl);
						String tempscheme = tempuri.getScheme();
						String temphost = tempuri.getHost();

						String temppath = tempuri.getPath();
						String tempqueryString = tempuri.getQuery() != null ? tempuri.getQuery() : "";

						StringTokenizer temp = new StringTokenizer(tempqueryString);

						if (temphost.equals("www.amazon.in")) {

							modiURL.append(tempscheme).append("://").append(temphost).append(temppath).append("?");

							while (temp.hasMoreTokens()) {
								String temptoken = temp.nextToken();
								if (!temptoken.startsWith("tag") && !temptoken.startsWith("affid")
										&& !temptoken.startsWith("sid")) {
									modiURL.append(temptoken);
									modiURL.append("&");
								}

							}

							modiURL.append("tag=shafunint-21");
							return "https://xtrashop.in/" + grapUtility.createShortURL(modiURL.toString());
						} else if (temphost.equals("www.flipkart.com")) {

							modiURL.append(tempscheme).append("://").append(temphost).append(temppath).append("?");

							while (temp.hasMoreTokens()) {
								String temptoken = temp.nextToken();
								if (!temptoken.startsWith("tag") && !temptoken.startsWith("affid")) {
									modiURL.append(temptoken);
									modiURL.append("&");
								}

							}

							modiURL.append("affid=shahinnpg");
							return "https://xtrashop.in/" + grapUtility.createShortURL(modiURL.toString());

						}

					}

				}

			}

			return "https://xtrashop.in/" + grapUtility.createShortURL(url);

		} catch (URISyntaxException e) {
			return url;
		}

	}

	
	
}